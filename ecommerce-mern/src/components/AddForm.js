import { Form, Button, Image } from 'react-bootstrap';
import { ProductContext } from '../contexts/ProductContext';
import { useContext, useState } from 'react';

const AddForm = () => {
  const { addProduct } = useContext(ProductContext);

  const [newProduct, setNewProduct] = useState({
    image: '',
    name: '',
    description: '',
    price: ''
  });

  const onInputChange = (e) => {
    setNewProduct({ ...newProduct, [e.target.name]: e.target.value });
  };

  const { image, name, description, price } = newProduct;

  const handleSubmit = (e) => {
    e.preventDefault();
    addProduct(image, name, description, price);
  };

  return (
    <Form className="d-flex flex-column gap-2" onSubmit={handleSubmit}>
      <Form.Group>
        <div
          style={{ width: 'auto', height: '300px' }}
          className="bg-light d-flex"
        >
          {image ? (
            <Image
              src={image}
              rounded
              fluid
              style={{ width: '100%', height: '100%', objectFit: 'cover' }}
            />
          ) : (
            <h6 className="m-auto">No Image</h6>
          )}
        </div>
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Image Link"
          name="image"
          value={image}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Product Name"
          name="name"
          value={name}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          as="textarea"
          rows={3}
          placeholder="Description"
          name="description"
          value={description}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="number"
          placeholder="Price"
          name="price"
          value={price}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Button variant="success" type="submit" block>
        Add New Product
      </Button>
    </Form>
  );
};

export default AddForm;
