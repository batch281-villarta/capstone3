import { useContext } from 'react';
import { OrderContext } from '../contexts/OrderContext';
import OrderItem from '../components/OrderItem';

function Orders() {
  const { myOrder } = useContext(OrderContext);

  // myOrder.map((i) => console.log(i));

  console.log(myOrder);
  return (
    <div className="container">
      <div className="row d-flex justify-content-center align-items-center">
        <div className="col-lg-12 ">
          <div className=" ">
            <h3 className="mb-4">Order History</h3>
            <div className="d-flex flex-column gap-4">
              {myOrder.reverse().map((order) => (
                <OrderItem orderDetails={order} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Orders;
