import { createContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export const CartContext = createContext();

const CartContextProvider = (props) => {
  const navigate = useNavigate();
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);

  const removeToCart = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        cartId: id
      })
      // headers: {
      //   Authorization: `Bearer ${localStorage.getItem('token')}`
      // }
    })
      .then((response) => response.json())
      .then((data) => {
        setTotal(data.total);
        setCart(data.cart);
      });
  };

  const updateCart = (cartId, quantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        cartId,
        quantity
      })
      // headers: {
      //   Authorization: `Bearer ${localStorage.getItem('token')}`
      // }
    })
      .then((response) => response.json())
      .then((data) => {
        setTotal(data.total);
        setCart(data.cart);
      });
  };

  const addToCart = (id, quantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: id,
        quantity
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 'error') {
          Swal2.fire({
            title: `Can't add to cart`,
            icon: 'error',
            text: 'Please login as user and try again'
          });
          navigate('/login');
        } else {
          retrieveCartDetails(localStorage.getItem('token'));
          Swal2.fire({
            title: 'Product Added to Cart',
            icon: 'success'
          });
        }
      });
  };

  const retrieveCartDetails = (token) => {
    return fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 'success') {
          setCart(data.cart.products);
          setTotal(data.cart.totalAmount);
        } else {
          setCart([]);
          setTotal(0);
        }

        // : setCart([]);)
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(typeof data.cart.products);
        if (data.status === 'success') {
          setCart(data.cart.products);
          setTotal(data.cart.totalAmount);
        } else {
          setCart([]);
          setTotal(0);
        }
      });
  }, []);

  return (
    <CartContext.Provider
      value={{
        cart,
        total,
        setTotal,
        setCart,
        addToCart,
        removeToCart,
        updateCart
      }}
    >
      {props.children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
