import { Button, Carousel, Container, Image, Row } from 'react-bootstrap';
import { images } from '../Links/Links.js';
import ActiveProductList from '../components/ActiveProductList.js';
import { Link } from 'react-router-dom';
import FeaturedProductList from '../components/FeaturedProductList.js';

function Home() {
  return (
    <>
      <Container fluid>
        <Row>
          <div className="col-12">
            <div
              className="position-relative overflow-hidden text-center bg-body-tertiary hero rounded d-flex justify-content-between align-items-center"
              style={{
                backgroundImage: `url(${images.hero3})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundPosition: 'center'
              }}
            >
              {/* <div className="col-12 d-flex flex-column align-items-center position-absolute text-white left-0">
                <h1 className="display-3 fw-bold">Yike Invincible 2023</h1>
                <h4 className="fw-normal text-white mb-3">
                  Cushioned comfort for every mile
                </h4>
                <div className="">
                  <Button
                    variant="dark"
                    type="submit"
                    size="md"
                    className="px-4 py-3 rounded-pill mt-2"
                    as={Link}
                    to="/products"
                  >
                    Explore Collection
                  </Button>
                </div>
              </div> */}
            </div>
          </div>
          <div className="col-12 d-flex flex-column align-items-center my-5">
            <h1 className="display-3 fw-bold">Yike Invincible 2023</h1>
            <h4 className="fw-normal text-muted mb-3">
              Cushioned comfort for every mile
            </h4>
            <div className="">
              <Button
                variant="dark"
                type="submit"
                size="md"
                className="px-4 py-3 rounded-pill mt-2"
                // as={Link}
                // to="/products"
                href="#products"
              >
                Explore Collection
              </Button>
            </div>
          </div>
        </Row>
        <Row>
          <div className="mt-5">
            <div className="d-flex justify-content-between align-items-end">
              <h4>Don't Miss</h4>
              <span>
                <Button
                  variant="outline-dark"
                  className="rounded-pill"
                  as={Link}
                  to="/featured"
                >
                  See All
                </Button>
              </span>
            </div>

            <FeaturedProductList />
          </div>
        </Row>

        <div className="mt-5" id="products">
          <div className="d-flex justify-content-between align-items-end">
            <h4>Always Iconic</h4>
            <span>
              <Button
                variant="outline-dark"
                className="rounded-pill"
                as={Link}
                to="/products"
              >
                See All
              </Button>
            </span>
          </div>

          <ActiveProductList />
        </div>

        <div
          className="mt-5"
          style={{
            width: '100%',
            height: '0',
            position: 'relative',
            paddingBottom: '41.875%'
          }}
        >
          <iframe
            src="https://streamable.com/e/le6wrn?autoplay=1&nocontrols=1"
            frameBorder="0"
            width="100%"
            height="100%"
            title="Video Ads"
            allowFullScreen
            allow="autoplay"
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              left: '0',
              top: '0',
              overflow: 'hidden'
            }}
          ></iframe>
        </div>
      </Container>
    </>
  );
}

export default Home;
