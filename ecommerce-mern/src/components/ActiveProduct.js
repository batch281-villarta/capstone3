import { Card } from 'react-bootstrap';
import { NumericFormat } from 'react-number-format';
import { Link } from 'react-router-dom';

function ActiveProduct({ product }) {
  return (
    <div>
      <Card
        className="border-0"
        as={Link}
        to={`/products/${product._id}`}
        style={{ textDecoration: 'none' }}
      >
        <div
          style={{ width: '100%', paddingTop: '100%', position: 'relative' }}
        >
          <Card.Img
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              objectFit: 'cover'
            }}
            variant="top"
            src={product.image}
            alt={product.description}
          />
        </div>
        <Card.Body className="d-flex justify-content-between mb-5">
          <div>
            <Card.Title>{product.name}</Card.Title>
            <Card.Text className="text-muted">{product.description}</Card.Text>
          </div>
          <Card.Title>
            <NumericFormat
              value={product.price.toFixed(2)}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'$'}
            />
          </Card.Title>
        </Card.Body>
      </Card>
    </div>
  );
}

export default ActiveProduct;
