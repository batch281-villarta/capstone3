import { useContext, useEffect, useRef, useState } from 'react';
import { Accordion, Button, Form, Image } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';

import ActiveProductList from './ActiveProductList';

import { CartContext } from '../contexts/CartContext';
import { NumericFormat } from 'react-number-format';

function ActiveProductView() {
  const { id } = useParams();

  const { addToCart } = useContext(CartContext);

  const [name, setName] = useState('');
  const [initialPrice, setInitialPrice] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');

  const [quantity, setQuantity] = useState(1);

  const productRef = useRef(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setInitialPrice(data.price);
        setPrice(data.price);
        setDescription(data.description);
        setImage(data.image);

        // Set focus on the product element after data is loaded
        if (productRef.current) {
          productRef.current.focus();
        }
      });

    setQuantity(1);
  }, [id]);

  useEffect(() => {
    const calculatedPrice = initialPrice * quantity;
    setPrice(calculatedPrice.toFixed(2));
  }, [quantity]);

  const handleAddToCart = () => {
    addToCart(id, quantity);
  };

  const quantityOptions = Array.from({ length: 10 }, (_, index) => (
    <option key={index + 1} value={index + 1}>
      {index + 1}
    </option>
  ));

  return (
    <div>
      <div className="container p-5">
        <div className="row mt-3" id="product" tabIndex={-1} ref={productRef}>
          <div className="col-lg-7">
            <div
              style={{
                width: '100%',
                paddingTop: '100%',
                position: 'relative'
              }}
            >
              <Image
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover'
                }}
                variant="top"
                src={image}
                alt={description}
              />
            </div>
          </div>
          <div className="col-lg-5">
            <div className="d-flex flex-column">
              <h1>{name}</h1>
              <h4>
                <NumericFormat
                  value={price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'}
                />
              </h4>
              <span className="mt-2">{description}</span>

              <div className="d-flex flex-column mt-4 gap-2">
                <div className="d-flex justify-content-between align-items-center">
                  <span className="w-75">Quantity</span>
                  <Form.Select
                    aria-label="Default select example"
                    value={quantity}
                    onChange={(e) => setQuantity(parseInt(e.target.value))}
                  >
                    {quantityOptions}
                  </Form.Select>
                  {/* <Form.Control
                    type="number"
                    defaultValue="1"
                    onChange={(e) => {
                      const value = parseInt(e.target.value);
                      if (value && value >= 1) {
                        setQuantity(value);
                      }
                    }}
                    min="1"
                  /> */}
                </div>
                <Button
                  variant="dark"
                  className="p-3 rounded-pill"
                  onClick={() => handleAddToCart(id)}
                >
                  Add to Cart
                </Button>

                {/*  */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-between align-items-end mt-5">
        <h4>You might also like</h4>
        <Button variant="" as={Link} to="/products">
          See All
        </Button>
      </div>
      <ActiveProductList id={id} />
    </div>
  );
}

export default ActiveProductView;
