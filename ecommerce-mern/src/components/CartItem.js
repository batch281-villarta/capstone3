/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useContext, useState } from 'react';
import { Form, Image } from 'react-bootstrap';
import { CartContext } from '../contexts/CartContext';
import { NumericFormat } from 'react-number-format';

function CartItem({ cart }) {
  const { removeToCart, updateCart } = useContext(CartContext);

  const quantityOptions = Array.from({ length: 10 }, (_, index) => {
    const optionValue = index + 1;
    return (
      <option key={optionValue} value={optionValue}>
        {optionValue}
      </option>
    );
  });

  //   const [quantity, setQuantity] = useState(0);

  const handleRemove = (id) => {
    removeToCart(id);
  };

  const handleChangeQuantity = (value) => {
    updateCart(cart._id, value);
  };

  return (
    <div className="row mb-3">
      <div
        className="col-6"
        style={{
          width: '200px',
          height: '200px',
          position: 'relative'
        }}
      >
        <Image
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            objectFit: 'cover'
          }}
          variant="top"
          src={cart.image}
          alt={cart.description}
        />
      </div>
      <div className=" col-6">
        <div className="d-flex flex-column ms-3">
          <div className="d-flex justify-content-between">
            <strong>{cart.productName}</strong>
            <strong>
              <NumericFormat
                value={cart.price.toFixed(2)}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'}
              />
            </strong>
          </div>
          <span className="mt-2 text-muted w-75">{cart.description}</span>

          <div className="d-flex flex-column mt-4 gap-2">
            <div className="d-flex justify-content-between align-items-center">
              <div className="d-flex align-items-center gap-2">
                Quantity
                <Form.Select
                  aria-label="Default select example"
                  className="w-50"
                  defaultValue={cart.quantity}
                  onChange={(e) => handleChangeQuantity(e.target.value)}
                >
                  {quantityOptions}
                </Form.Select>
              </div>
              <a
                className="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover"
                style={{ cursor: 'pointer' }}
                onClick={() => handleRemove(cart._id)}
              >
                Remove
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CartItem;
