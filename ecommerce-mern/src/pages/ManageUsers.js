import { useContext } from 'react';
import { UserContext } from '../contexts/UserContext';
import { useNavigate } from 'react-router-dom';
import UserList from '../components/UserList';

function ManageUsers() {
  const { user, allUsers } = useContext(UserContext);
  const navigate = useNavigate();

  console.log(allUsers);

  console.log(user);
  return user.isAdmin ? (
    <div className="container-xl">
      <div className="table-responsive">
        <div className="table-wrapper">
          <UserList />
        </div>
      </div>
    </div>
  ) : (
    navigate('/')
  );
}

export default ManageUsers;
