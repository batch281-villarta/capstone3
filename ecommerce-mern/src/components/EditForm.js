import { Form, Button, Image } from 'react-bootstrap';
import { ProductContext } from '../contexts/ProductContext';
import { useContext, useState } from 'react';

const EditForm = ({ theProduct }) => {
  const id = theProduct._id;

  const [name, setName] = useState(theProduct.name);
  const [description, setDescription] = useState(theProduct.description);
  const [price, setPrice] = useState(theProduct.price);
  const [image, setImage] = useState(theProduct.image);

  const { updateProduct } = useContext(ProductContext);

  const handleSubmit = (e) => {
    e.preventDefault();

    updateProduct(id, image, name, description, price);
  };

  return (
    <Form className="d-flex flex-column gap-2" onSubmit={handleSubmit}>
      <Form.Group>
        <div style={{ width: 'auto', height: '300px' }}>
          <Image
            src={image}
            rounded
            fluid
            style={{ width: '100%', height: '100%', objectFit: 'cover' }}
          />
        </div>
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Image Link"
          name="image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Product Name"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          as="textarea"
          rows={3}
          placeholder="Description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          name="description"
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="number"
          placeholder="Price"
          name="price"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />
      </Form.Group>

      <Button variant="success" type="submit" block>
        Edit Product
      </Button>
    </Form>
  );
};

export default EditForm;
