import React from 'react';
import { Badge, ListGroup } from 'react-bootstrap';

function ProductOrderItem({ theProduct }) {
  const userOrders = theProduct.userOrders;
  return (
    <div className="d-flex flex-column gap-2">
      {userOrders.map((user) => (
        <ListGroup key={user._id}>
          <ListGroup.Item className="d-flex justify-content-between align-items-start">
            <div className="fw-bold">
              {user.lastName}, {user.firstName}
            </div>

            <Badge className="bg-success-subtle text-success-emphasis p-2" pill>
              {user.email}
            </Badge>
          </ListGroup.Item>
        </ListGroup>
      ))}
    </div>
  );
}

export default ProductOrderItem;
