import React, { useContext, useReducer } from 'react';
import { Button, Form } from 'react-bootstrap';

import { UserContext } from '../contexts/UserContext';

import { images } from '../Links/Links.js';
import { useNavigate } from 'react-router-dom';

const initialState = {
  firstName: '',
  lastName: '',
  mobileNumber: '',
  email: '',
  password: ''
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_FIRST_NAME':
      return { ...state, firstName: action.payload };
    case 'SET_LAST_NAME':
      return { ...state, lastName: action.payload };
    case 'SET_MOBILE_NUMBER':
      return { ...state, mobileNumber: action.payload };
    case 'SET_EMAIL':
      return { ...state, email: action.payload };
    case 'SET_PASSWORD':
      return { ...state, password: action.payload };
    default:
      return state;
  }
};

function Login() {
  const { user, register } = useContext(UserContext);
  const [state, dispatch] = useReducer(reducer, initialState);

  const navigate = useNavigate();

  const { firstName, lastName, mobileNumber, email, password } = state;

  const handleLoginSubmit = (e) => {
    e.preventDefault();
    console.log(state);
    register(firstName, lastName, mobileNumber, email, password);
  };

  const imageUrl = images.hero2;

  return user.id === null || user.id === undefined ? (
    <div
      className="bg-success fullscreen d-flex justify-content-center align-items-center w-100 m-auto "
      style={{
        backgroundImage: `url(${imageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'top'
      }}
    >
      <div className="w-25 bg-light p-4 rounded">
        <Form onSubmit={handleLoginSubmit} className="d-flex flex-column gap-2">
          <h1 className="h3 mb-3 fw-normal">Register</h1>

          {/* Firstname */}
          <Form.Group>
            <div className="form-floating">
              <Form.Control
                placeholder="First Name"
                type="text"
                value={firstName}
                onChange={(e) =>
                  dispatch({ type: 'SET_FIRST_NAME', payload: e.target.value })
                }
                required
              />
              <label>First Name</label>
            </div>
          </Form.Group>

          {/* Lastname */}
          <Form.Group>
            <div className="form-floating">
              <Form.Control
                placeholder="Last Name"
                type="text"
                value={lastName}
                onChange={(e) =>
                  dispatch({ type: 'SET_LAST_NAME', payload: e.target.value })
                }
                required
              />
              <label>Last Name</label>
            </div>
          </Form.Group>

          {/* Mobile Number */}

          <Form.Group>
            <div className="form-floating">
              <Form.Control
                placeholder="+63 (022) 265 8932"
                type="number"
                value={mobileNumber}
                onChange={(e) =>
                  dispatch({
                    type: 'SET_MOBILE_NUMBER',
                    payload: e.target.value
                  })
                }
                required
              />
              <label>Mobile Number</label>
            </div>
          </Form.Group>

          {/* Email */}
          <Form.Group controlId="userEmail">
            <div className="form-floating">
              <Form.Control
                type="email"
                placeholder="name@example.com"
                value={email}
                onChange={(e) =>
                  dispatch({
                    type: 'SET_EMAIL',
                    payload: e.target.value
                  })
                }
                required
              />
              <label>Email address</label>
            </div>
          </Form.Group>

          {/* Password */}
          <Form.Group controlId="password">
            <div className="form-floating">
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) =>
                  dispatch({
                    type: 'SET_PASSWORD',
                    payload: e.target.value
                  })
                }
                required
              />
              <label>Password</label>
            </div>
          </Form.Group>

          <Button
            variant="dark"
            type="submit"
            className="p-2 w-100 rounded-pill py-3 mt-3"
          >
            Register
          </Button>
        </Form>
      </div>
    </div>
  ) : (
    navigate('/')
  );
}

export default Login;
