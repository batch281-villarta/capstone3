/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { LockFill } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';

import { OrderContext } from '../contexts/OrderContext';
import { CartContext } from '../contexts/CartContext';
import OrderSummary from '../components/OrderSummary';
import { NumericFormat } from 'react-number-format';

import { images } from '../Links/Links.js';
import ActiveProductList from '../components/ActiveProductList';

function Checkout() {
  const { checkout } = useContext(OrderContext);
  const { cart, total } = useContext(CartContext);

  const imageUrl = images.hero;

  console.log(cart);

  const handleCheckOut = () => {
    checkout();
  };
  return (
    <div>
      <div
        className="bg-success fullscreen d-flex justify-content-center align-items-center w-100 m-auto "
        style={{
          backgroundImage: `url(${imageUrl})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          backgroundPosition: 'top'
        }}
      >
        <div className="container mt-4">
          <div className="row">
            <div className="col-lg-6 w-50 mx-auto mb-5 ">
              <div className=" p-4 d-flex flex-column gap-1 border mt-5 rounded bg-light">
                <div className="mb-3 ">
                  <Button variant="Link" as={Link} to="/cart" className="p-0">
                    Cart
                  </Button>
                  <span className="p-2">/</span>
                  <Button variant="Link" className="p-0">
                    Checkout
                  </Button>
                </div>
                <h4 className="mb-4">Order Summary</h4>
                {cart.map((product) => (
                  <div key={product.productId}>
                    <OrderSummary product={product} />
                  </div>
                ))}

                <hr className="divider" />
                <div className="d-flex justify-content-between">
                  <strong>Total</strong>
                  <strong>
                    <NumericFormat
                      value={total.toFixed(2)}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'$'}
                    />
                  </strong>
                </div>

                <Button
                  variant="dark"
                  className="p-3 mt-3 d-flex justify-content-center gap-2 align-items-center rounded-pill"
                  onClick={handleCheckOut}
                >
                  <LockFill /> Pay
                  <NumericFormat
                    value={total.toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'$'}
                  />
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-5">
        <div className="d-flex justify-content-between align-items-end">
          <h4>You might also like</h4>
          <span>
            <Button variant="" as={Link} to="/products">
              See All
            </Button>
          </span>
        </div>
        <ActiveProductList />
      </div>
    </div>

    //
  );
}

export default Checkout;
