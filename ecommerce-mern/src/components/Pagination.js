import { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';

const Pagination = ({
  pages,
  setCurrentPage,
  currentProducts,
  numberOfProducts
}) => {
  const numOfPages = [];

  for (let i = 1; i <= pages; i++) {
    numOfPages.push(i);
  }

  const [currentButton, setCurrentButton] = useState(1);

  useEffect(() => {
    setCurrentPage(currentButton);
  }, [currentButton, setCurrentPage]);

  return (
    <div className="d-flex justify-content-between">
      <div className="hint-text ">
        Showing <b>{currentProducts.length}</b> out of <b>{numberOfProducts}</b>{' '}
        products
      </div>
      <ul className="pagination d-flex gap-1  align-items-center">
        <li
          className={`${
            currentButton === 1 ? 'page-item disabled' : 'page-item'
          }`}
        >
          <Button
            variant="light"
            size="sm"
            onClick={() =>
              setCurrentButton((prev) => (prev === 1 ? prev : prev - 1))
            }
          >
            Previous
          </Button>
        </li>

        {numOfPages.map((page, index) => {
          return (
            <li
              key={index}
              className={`${
                currentButton === page ? 'page-item active' : 'page-item'
              }`}
            >
              <Button
                variant="outline-secondary"
                size="sm"
                onClick={() => setCurrentButton(page)}
              >
                {page}
              </Button>
            </li>
          );
        })}

        <li
          className={`${
            currentButton === numOfPages.length
              ? 'page-item disabled'
              : 'page-item'
          }`}
        >
          <Button
            variant="light"
            size="sm"
            onClick={() =>
              setCurrentButton((next) =>
                next === numOfPages.length ? next : next + 1
              )
            }
          >
            Next
          </Button>
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
