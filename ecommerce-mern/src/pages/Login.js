import React, { useContext, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { images } from '../Links/Links.js';

import { UserContext } from '../contexts/UserContext';
import { useNavigate } from 'react-router-dom';

function Login() {
  const { authenticate, user } = useContext(UserContext);

  const navigate = useNavigate();

  const imageUrl = images.hero3;

  console.log('user', user);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLoginSubmit = (e) => {
    e.preventDefault();
    authenticate(email, password);
  };

  return user.id === null || user.id === undefined ? (
    <div
      className="bg-success fullscreen d-flex justify-content-center align-items-center w-100 m-auto "
      style={{
        backgroundImage: `url(${imageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'top'
      }}
    >
      <div className="w-25 bg-light p-4 rounded">
        <Form onSubmit={handleLoginSubmit} className="d-flex flex-column gap-2">
          <h1 className="h3 mb-3 fw-normal">Sign in</h1>
          <Form.Group controlId="userEmail">
            <div className="form-floating">
              <Form.Control
                type="email"
                placeholder="name@example.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <label>Email address</label>
            </div>
          </Form.Group>

          <Form.Group controlId="password">
            <div className="form-floating">
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
              <label>Password</label>
            </div>
          </Form.Group>

          <Button
            variant="dark"
            type="submit"
            className="p-2 w-100 rounded-pill py-3 mt-3"
          >
            Login
          </Button>
        </Form>
      </div>
    </div>
  ) : (
    navigate('/')
  );
}

export default Login;
